Source: phonon
Section: sound
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
Build-Depends: dpkg-dev (>= 1.22.5), cmake (>= 3.5~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.90~),
               libglib2.0-dev,
               libpulse-dev (>= 0.9.21),
               pkg-kde-tools (>= 0.12),
               qt6-base-dev,
               qt6-5compat-dev,
               qt6-tools-dev,
               qtbase5-dev (>= 5.15.0~),
               qttools5-dev (>= 5.15.0~),
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://phonon.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/phonon
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/phonon.git

Package: libphonon-l10n
Architecture: all
Multi-Arch: foreign
Section: localization
Depends: ${misc:Depends}
Breaks: libphonon4qt5-data (<< 4:4.12.0-2~)
Replaces: libphonon4qt5-data (<< 4:4.12.0-2~)
Description: multimedia framework from KDE using Qt - localization files
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the shared localizations for the core Phonon library.

Package: libphonon4qt5-4t64
Provides: ${t64:Provides}
Replaces: libphonon4qt5-4
Breaks: libphonon4qt5-4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: libphonon-l10n (>= ${source:Version}),
         ${misc:Depends}, ${shlibs:Depends}
Description: multimedia framework from KDE using Qt 5 - core library
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the core library of the Phonon cross-platform multimedia
 framework from KDE. It is required to run applications that use Phonon.

Package: libphonon4qt5-data
Architecture: all
Multi-Arch: foreign
Section: oldlibs
Depends: libphonon-l10n (>= ${source:Version}),
         ${misc:Depends}
Description: transitional package for libphonon-l10n
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This is a transitional package for migrating to libphonon-l10n. It can safely
 be removed.

Package: libphonon4qt5-dev
Architecture: any
Section: libdevel
Depends: libphonon4qt5-4t64 (= ${binary:Version}),
         phonon4qt5-backend-null | phonon4qt5,
         ${misc:Depends},
         ${shlibs:Depends},
Description: multimedia framework from KDE using Qt 5 - development files
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the development files needed to build Qt 5 applications
 using the Phonon cross-platform multimedia framework.

Package: libphonon4qt5experimental-dev
Architecture: any
Section: libdevel
Depends: libphonon4qt5experimental4t64 (= ${binary:Version}), ${misc:Depends}
Breaks: libphonon4qt5-dev (<< 4:4.11.1~)
Replaces: libphonon4qt5-dev (<< 4:4.11.1~)
Description: multimedia framework from KDE using Qt 5 - experimental development files
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the development files needed to build Phonon back-ends
 or internal applications which need the Phonon-Experimental library.
 Third-party applications should not depend on this library. Please read
 README.Debian if you are considering a build-dependency on this package.

Package: libphonon4qt5experimental4t64
Replaces: libphonon4qt5experimental4
Breaks: libphonon4qt5experimental4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: libphonon4qt5-4t64 (= ${binary:Version}),
         ${misc:Depends}, ${shlibs:Depends}
Provides: ${t64:Provides}, phonon4qt5experimental-abi-1
Description: multimedia framework from KDE using Qt 5 - experimental library (internal)
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the Phonon-Experimental library, which might be needed
 by Phonon back-ends or other internal applications. Third-party applications
 should not depend on this library. Please read README.Debian before using this
 library.

Package: phonon4qt5
Architecture: any
Multi-Arch: same
Depends: libphonon4qt5-4t64 (>= ${source:Version}),
         ${misc:Depends},
         ${phonon:Recommended-Backend-qt5} | phonon4qt5-backend,
Suggests: phonon4qt5-backend-gstreamer, phonon4qt5-backend-vlc
Description: multimedia framework from KDE using Qt 5 - metapackage
 Phonon Qt 5 is a multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This metapackage ensures that the system has a working Phonon configuration
 installed (the core library and at least one back-end).

Package: phonon4qt5-backend-null
Architecture: any
Multi-Arch: same
Provides: phonon4qt5-backend
Depends: libphonon4qt5-4t64, ${misc:Depends}
Conflicts: phonon4qt5-backend
Description: multimedia framework from KDE using Qt 5 - null back-end (no real back-end)
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package does not provide any real Phonon back-end. It can be used to
 disable Phonon audio/video capabilities. Please note that Phonon behavior
 with this package installed has not been well tested.

Package: phonon4qt5settings
Architecture: any
Depends: libphonon4qt5-4t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: multimedia framework from KDE using Qt 5 - settings application
 Phonon is a Qt 5 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains a graphical configuration for the Phonon cross-platform
 multimedia framework.

Package: libphonon4qt6-4t64
Provides: ${t64:Provides}
Replaces: libphonon4qt6-4
Breaks: libphonon4qt6-4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: libphonon-l10n (>= ${source:Version}),
         ${misc:Depends}, ${shlibs:Depends}
Description: multimedia framework from KDE using Qt 6 - core library
 Phonon is a Qt 6 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the core library of the Phonon cross-platform multimedia
 framework from KDE. It is required to run applications that use Phonon.

Package: libphonon4qt6-dev
Architecture: any
Section: libdevel
Depends: libphonon4qt6-4t64 (= ${binary:Version}),
         phonon4qt6-backend-null | phonon4qt6,
         ${misc:Depends},
         ${shlibs:Depends},
Description: multimedia framework from KDE using Qt 6 - development files
 Phonon is a Qt 6 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the development files needed to build Qt 6 applications
 using the Phonon cross-platform multimedia framework.

Package: libphonon4qt6experimental-dev
Architecture: any
Section: libdevel
Depends: libphonon4qt6experimental4t64 (= ${binary:Version}), ${misc:Depends}
Description: multimedia framework from KDE using Qt 6 - experimental development files
 Phonon is a Qt 6 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the development files needed to build Phonon back-ends
 or internal applications which need the Phonon-Experimental library.
 Third-party applications should not depend on this library. Please read
 README.Debian if you are considering a build-dependency on this package.

Package: libphonon4qt6experimental4t64
Replaces: libphonon4qt6experimental4
Breaks: libphonon4qt6experimental4 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: libphonon4qt6-4t64 (= ${binary:Version}),
         ${misc:Depends}, ${shlibs:Depends}
Provides: ${t64:Provides}, phonon4qt6experimental-abi-1
Description: multimedia framework from KDE using Qt 6 - experimental library (internal)
 Phonon is a Qt 6 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package contains the Phonon-Experimental library, which might be needed
 by Phonon back-ends or other internal applications. Third-party applications
 should not depend on this library. Please read README.Debian before using this
 library.

Package: phonon4qt6
Architecture: any
Multi-Arch: same
Depends: libphonon4qt6-4t64 (>= ${source:Version}),
         ${misc:Depends},
         ${phonon:Recommended-Backend-qt6} | phonon4qt6-backend,
Suggests: phonon4qt6-backend-gstreamer, phonon4qt6-backend-vlc
Description: multimedia framework from KDE using Qt 6 - metapackage
 Phonon Qt 6 is a multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This metapackage ensures that the system has a working Phonon configuration
 installed (the core library and at least one back-end).

Package: phonon4qt6-backend-null
Architecture: any
Multi-Arch: same
Provides: phonon4qt6-backend
Depends: libphonon4qt6-4t64, ${misc:Depends}
Conflicts: phonon4qt6-backend
Description: multimedia framework from KDE using Qt 6 - null back-end (no real back-end)
 Phonon is a Qt 6 multimedia API, which provides a task-oriented abstraction
 layer for capturing, mixing, processing, and playing audio and video content.
 .
 This package does not provide any real Phonon back-end. It can be used to
 disable Phonon audio/video capabilities. Please note that Phonon behavior
 with this package installed has not been well tested.
